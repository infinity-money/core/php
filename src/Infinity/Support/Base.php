<?php namespace Infinity\Support;

/**
 * Infinity Core PHP Library
 * Author: Irman Ahmad
 * Date: 2018-04-18
 * Time: 4:09 PM
 */

class Base
{
    static function RIS(&$var, $altVal = '')
    {
        if (isset($var)) {
            $newVar = $var;
            if (is_string($newVar)) {
                $newVar = trim($newVar);
            }
            if (!empty($newVar)) {
                return $newVar;
            } else {
                return $altVal;
            }
        } else {
            return $altVal;
        }
    }
}