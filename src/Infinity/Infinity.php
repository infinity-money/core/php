<?php namespace Infinity;

/**
 * Infinity Core PHP Library
 *
 * @author Irman Ahmad
 *
 */

class Infinity
{
    public static $publicKey;
    public static $secretKey;
    
    static function setKeys($publicKey, $secretKey){
        self::setPublicKey($publicKey);
        self::setSecretKey($secretKey);
    }

    #region Setters
    /**
     * @param string $publicKey
     */
    public static function setPublicKey($publicKey)
    {
        self::$publicKey = $publicKey;
    }
    /**
     * @param string $secretKey
     */
    public static function setSecretKey($secretKey)
    {
        self::$secretKey = $secretKey;
    }    
    #endregion

    static function verifyNotificationSignature($orderReference, $status, $signature){
        $data = [
            self::$publicKey,
            $orderReference,
            $status
        ];
        $realSignature = base64_encode(hash_hmac('sha256', implode('~', $data), self::$secretKey, true));

        return $signature === $realSignature;
    }
}