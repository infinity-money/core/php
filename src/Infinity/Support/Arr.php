<?php namespace Infinity\Support;

/**
 * Infinity Core PHP Library
 * Author: Irman Ahmad
 * Date: 2018-04-18
 * Time: 3:32 PM
 */

use ArrayAccess;
use Closure;

class Arr
{
    static function value($value)
    {
        return $value instanceof Closure ? $value() : $value;
    }
    static function data_get($target, $key, $default = null)
    {
        if (is_null($key)) return $target;
        foreach (explode('.', $key) as $segment)
        {
            if (is_array($target))
            {
                if ( ! array_key_exists($segment, $target))
                {
                    return self::value($default);
                }
                $target = $target[$segment];
            }
            elseif ($target instanceof ArrayAccess)
            {
                if ( ! isset($target[$segment]))
                {
                    return self::value($default);
                }
                $target = $target[$segment];
            }
            elseif (is_object($target))
            {
                if ( ! isset($target->{$segment}))
                {
                    return self::value($default);
                }
                $target = $target->{$segment};
            }
            else
            {
                return self::value($default);
            }
        }
        return $target;
    }
}